const COMMON = {
	ERROR: {
		OFFSET: 3
	},
	TYPE_ERROR: {
		MESSAGE: "Wrong argument type for function"
	},
	RANGE_ERROR: {
		MESSAGE: "Argument out of range for function"
	}
};

/**
 * Gets the type of the provided value.
 *
 * @param {*} value - Any possible value
 * @returns {string} Type of the value: Object, Boolean, Number, String, Array, Date, Null, Undefined, Error, ...
 */
function getType(value) {
	return Object.prototype.toString.call(value).replace(/^\[object |\]$/g, "");
}

/**
 * Converts time to the correct multiple.
 *
 * @param {number} time - Time in milliseconds
 * @returns {string} Converted time
 * @throws {TypeError} Parameter must be a number
 */
function convertTime(time) {
	if (getType(time) === "Number") {
		return time >= 1000 ? `${time / 1000}s` : `${time}ms`;
	} else {
		throw new TypeError(`${COMMON.TYPE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
	}
}

/**
 * Gets the function name from the function where this is called.
 *
 * @param {Error} error - Example: new Error()
 * @returns {string} Name of the caller function
 * @throws {TypeError} Parameter must be an error object
 */
function getFunctionNameFromInside(error) {
	// arguments.callee.name is forbidden in ES5+ strict mode
	if (getType(error) === "Error") {
		let functionName = error.stack.split(/\r\n|\r|\n/g)[1].trim();
		functionName = functionName.substr(COMMON.ERROR.OFFSET, functionName.indexOf("(") - 1 - COMMON.ERROR.OFFSET);
		return functionName;
	} else {
		throw new TypeError(`${COMMON.TYPE_ERROR.MESSAGE} getFunctionNameFromInside`);
	}
}

/**
 * Checks if the service responds with the correct status. Aborts the test flow if there are infrastructural issues.
 *
 * @param {number} statusCode - Code of the response status
 * @throws {TypeError} Parameter must be a number
 * @throws {RangeError} Parameter must be an existing status code number
 */
function checkStatusCode(statusCode) {
	if (getType(statusCode) === "Number") {
		const ERROR_CODES = [503, 500, 502, 504, 401, 403];
		let descriptionStatusCode = "Status Code ";
		switch (true) {
			case (100 <= statusCode && statusCode <= 199):
				descriptionStatusCode += "(Information)";
				break;
			case (200 <= statusCode && statusCode <= 299):
				descriptionStatusCode += "(Success)";
				break;
			case (300 <= statusCode && statusCode <= 399):
				descriptionStatusCode += "(Redirection)";
				break;
			case (400 <= statusCode && statusCode <= 499):
				descriptionStatusCode += "(Client Error)";
				break;
			case (500 <= statusCode && statusCode <= 599):
				descriptionStatusCode += "(Server Error)";
				break;
			default:
				throw new RangeError(`${COMMON.RANGE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
		}
		pm.test(descriptionStatusCode, () => {
			pm.response.to.have.status(statusCode);
		});
		if (pm.response.code != statusCode) {
			for (let i = 0; i < ERROR_CODES.length; i++) {
				if (pm.response.code === ERROR_CODES[i]) {
					postman.setNextRequest(null);
					break;
				}
			}
		}
	} else {
		throw new TypeError(`${COMMON.TYPE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
	}
}

/**
 * Checks if the service responds within the required response time.
 *
 * @param {number} time - Elapsed time of the response
 * @throws {TypeError} Parameter must be a number
 * @throws {RangeError} Parameter must be a strictly positive number
 */
function checkTime(time) {
	if (getType(time) === "Number") {
		if (time > 0) {
			pm.test(`Response Time < ${convertTime(time)}`, () => {
				pm.expect(pm.response.responseTime).to.be.below(time);
			});
		} else {
			throw new RangeError(`${COMMON.RANGE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
		}
	} else {
		throw new TypeError(`${COMMON.TYPE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
	}
}

/**
 * Checks if the response body is structured conform the defined JSON schema.
 *
 * @param {Object} jsonSchema - JSON schema of the response body
 * @throws {TypeError} Parameter must be an object
 */
function checkJSONSchema(jsonSchema) {
	if (getType(jsonSchema) === "Object") {
		const VALID = tv4.validate(pm.response.json(), jsonSchema),
			  DESCRIPTION_JSON_SCHEMA = VALID ? "JSON Schema" : `JSON Schema (${tv4.error.message} for data path ${tv4.error.dataPath ? tv4.error.dataPath : "/"})`;
		pm.test(DESCRIPTION_JSON_SCHEMA, () => {
			pm.expect(VALID).to.be.true;
		});
	} else {
		throw new TypeError(`${COMMON.TYPE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
	}
}

/**
 * Generates a random number. Positive and negative numbers are allowed.
 *
 * @param {number} min - Minimum number (included)
 * @param {number} max - Maximum number (included)
 * @returns {number} Random number that ranges from min to max
 * @throws {TypeError} Parameters must be numbers
 * @throws {RangeError} Parameter max must be greater than min
 */
function generateNumber(min, max) {
	if (getType(min) === "Number" && getType(max) === "Number") {
		if (min <= max) {
			return Math.floor(Math.random() * (max - min + 1) + min);
		} else {
			throw new RangeError(`${COMMON.RANGE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
		}
	} else {
		throw new TypeError(`${COMMON.TYPE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
	}
}

/**
 * Generates a random string of characters.
 *
 * @param {number} length - Amount of characters to be generated
 * @returns {string} Text with random characters
 * @throws {TypeError} Parameter must be a number
 */
function generateString(length) {
	if (getType(length) === "Number") {
		const CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		let text = "";
		for (let i = 0; i < length; i++) {
			text += CHARACTERS.charAt(Math.floor(Math.random() * CHARACTERS.length));
		}
		return text;
	} else {
		throw new TypeError(`${COMMON.TYPE_ERROR.MESSAGE} ${getFunctionNameFromInside(new Error())}`);
	}
}