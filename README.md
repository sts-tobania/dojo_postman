# Dojo_Postman

<a href="https://www.postman.com/" target="_blank">
<p align="center" width="100%">
    <img width="75%" src="img/Postman_logo.png"> 
</p> </a>

**Introduction**

 0. <a href="https://www.postman.com/downloads" target="_blank">Download</a> 
 1. Workspace's
 2. Collection's
 3. Environment's
 4. Collection Runner
 5. Console
 6. many more ... ;-)

**Swagger**
<a href="http://tts-lb.westeurope.cloudapp.azure.com:8080/booking/swagger-ui.html" target="_blank">
<p align="center" width="100%">
    <img width="75%" src="img/swagger_logo.png">
</p> </a>

***Use Cases - Postman for Testers***

****Automated Testing****
* Understand API
* Build Test Suites
* Work & Verify with Team 
* Integrate in you Devops pipeline

****Exploratory Testing****
* Import Postman Collection / API spec's / Curl
* Explore your API / Create or chain requests /  Validate requests
* Colaborate on collections / Knowledge transfer

**Exercises**

baseURL = http://tts-lb.westeurope.cloudapp.azure.com:8080

****Exercise_1****
 * Create booking and verify

****Exercise_2****
 * Update a Booking and verify
    * tip: Cookie 
    * Advanced Lib Usage ? phoneNumber

****Exercise_3****
 * Delete Booking and verify

****Advanced usage****

import lib url in Postman (tip GET url and save in env variables. eval(\<environmentvarname>)

<https://gitlab.com/sts-tobania/dojo_postman/-/raw/master/Lib/postman-dojo-library.js>

****References****

<a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status" target="_blank">
<p align="center" width="100%">
    <img width="75%" src="img/HTTPstatusCodes.jpeg"> 
</p> </a>

**HTTP Methods**
<a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods" target="_blank">
<p align="center" width="100%">
    <img width="75%" src="img/HTTPmethods.png"> 
</p> </a>

**Postman References**
  * Intro
<https://learning.postman.com/docs/getting-started/introduction/>
  * Dynamic Variables
<https://learning.postman.com/docs/writing-scripts/script-references/variables-list/>
  * Variables Manage / Usage / Scope
<https://learning.postman.com/docs/sending-requests/variables/>

